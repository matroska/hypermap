package org.hypermap

import java.util.concurrent.{ConcurrentHashMap, ConcurrentMap}

import com.itv.lifecycle.Lifecycle
import org.scalatest.FreeSpec
import org.scalatest.Matchers._

import scala.collection.mutable.ListBuffer
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

class MemoryFileHyperMapSpec extends FreeSpec {

  "provides put, get, remove and hyperMapSize methods" in new TestContext {
    withHyperMap { hyperMap =>
      val key   = Key("ok")
      val value = Value(0)
      hyperMap += (key, value)
      hyperMap.get(key) should ===(Some(value))

      hyperMap.hyperMapSize should ===(HyperMapSize(1, 0))
      hyperMap.remove(key) should ===(Some(value))
      hyperMap.hyperMapSize should ===(HyperMapSize(0, 0))
    }
  }

  "uses memory map if size is below a 'file set limit' and retrieves element from it" in new TestContext {
    withHyperMap { hyperMap =>
      (1 to 4).foreach(number => hyperMap += (Key(number.toString), Value(number)))

      hyperMap.memoryMap should have size 4
      hyperMap.fileMap should have size 0

      hyperMap.get(Key("1")) should ===(Some(Value(1)))
    }
  }

  "uses file map for elements exceeding the 'file set limit' and retrieves elements from it" in new TestContext {
    withHyperMap { hyperMap =>
      (1 to 5).foreach(number => hyperMap += (Key(number.toString), Value(number)))

      hyperMap.hyperMapSize should ===(HyperMapSize(4, 1))
      hyperMap.fileMap.containsKey(Key("5")) shouldBe true

      hyperMap.get(Key("1")) should ===(Some(Value(1)))
      hyperMap.get(Key("5")) should ===(Some(Value(5)))
    }
  }

  "moves elements in file map into memory map when 'memory set limit' is reached" in new TestContext {
    withHyperMap { hyperMap =>
      (1 to 5).foreach(number => hyperMap += (Key(number.toString), Value(number)))

      hyperMap.hyperMapSize should ===(HyperMapSize(4, 1))
      hyperMap.fileMap.containsKey(Key("5")) shouldBe true

      hyperMap.remove(Key("1"))
      hyperMap.remove(Key("2"))
      hyperMap.remove(Key("3"))

      hyperMap.hyperMapSize should ===(HyperMapSize(2, 0))
    }
  }

  //inserisci elementi rimuovi elementi e continui a muovere elementi in background, quando operi in background, blocca solo il put e poi alla fine pulisci il file

  "moves elements in file map into memory map when 'memory set limit' is reached blocking every operation" in new TestContext {
    import scala.concurrent.duration.DurationInt
    val hyperMap = new MemoryFileHyperMap[Key, Value](new WaitMemoryMap[Key, Value](), new WaitFileMap[Key, Value](250), moveToMemoryLimit, moveToFileLimit)

    (1 to 5).foreach(number => hyperMap += (Key(number.toString), Value(number)))

    hyperMap.hyperMapSize should ===(HyperMapSize(4, 1))
    hyperMap.remove(Key("1"))
    hyperMap.remove(Key("2"))
    val futureRemove = Future {
      hyperMap.remove(Key("3"))
    }
    Future {
      Thread.sleep(50)
      hyperMap.get(Key("4"))
      hyperMap += (Key("6"), Value(6))
    }

    Await.result(futureRemove, 5.seconds)
    // hyperMap.hyperMapSize should ===(HyperMapSize(5, 1))
    commands should have size 11
    commands should ===(ListBuffer(MemoryCommand("put"),
      MemoryCommand("put"), MemoryCommand("put"),
      MemoryCommand("put"), FileCommand("put"),
      MemoryCommand("remove"), MemoryCommand("remove"),
      MemoryCommand("remove"), MemoryCommand("get"),
      FileCommand("clear"), MemoryCommand("put")))
  }

  trait TestContext {
    protected val moveToMemoryLimit = 2
    protected val moveToFileLimit   = 4

    protected val commands: ListBuffer[Command] = ListBuffer.empty

    def withHyperMap(fn: MemoryFileHyperMap[Key, Value] => Unit) = {
      Lifecycle.using(MemoryFileHyperMapFactory.make[Key, Value](moveToMemoryLimit, moveToFileLimit)) { memoryMap =>
        fn(memoryMap)
      }
    }

    sealed trait Command
    case class MemoryCommand(method: String) extends Command
    case class FileCommand(method: String) extends Command

    class WaitMemoryMap[K, V] extends scala.collection.mutable.HashMap[K, V] {
      override def get(key: K): Option[V] = {
        commands += MemoryCommand("get")
        super.get(key)
      }

      override def put(key: K, value: V): Option[V] = {
        commands += MemoryCommand("put")
        super.put(key, value)
      }

      override def remove(key: K): Option[V] = {
        commands += MemoryCommand("remove")
        super.remove(key)
      }
    }

    class WaitFileMap[K, V](clearMillisToWait: Long) extends ConcurrentHashMap[K, V] {
      override def get(key: scala.Any): V = {
        commands += FileCommand("get")
        super.get(key)
      }

      override def put(key: K, value: V): V = {
        commands += FileCommand("put")
        super.put(key, value)
      }

      override def clear(): Unit = {
        Thread.sleep(clearMillisToWait)
        commands += FileCommand("clear")
        super.clear()
      }
    }
  }
}
