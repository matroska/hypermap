package org.hypermap

import java.util.concurrent.{ConcurrentHashMap, Executors}

import com.itv.lifecycle.Lifecycle
import org.scalacheck.{Arbitrary, Gen}
import org.scalatest.FreeSpec
import org.scalacheck.ScalacheckShapeless._

import org.scalatest.Matchers._
import org.scalatest.prop.GeneratorDrivenPropertyChecks

import scala.collection.JavaConverters._

sealed trait Operation

case class Put(key: Key, value: Value) extends Operation

case class Delete(key: Key) extends Operation

object Generators {
  val numGen: Gen[Int]        = Gen.chooseNum[Int](0, 20)
  implicit val keyArbitrary   = Arbitrary(numGen.map(num => Key(num.toString)))
  implicit val valueArbitrary = Arbitrary(numGen.map(el => Value(el)))
}

class MemoryFileHyperMapIntegrationSpec extends FreeSpec with GeneratorDrivenPropertyChecks {

  //implicit val executionContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(5))
  implicit override val generatorDrivenConfig = PropertyCheckConfiguration(minSuccessful = 30)

  import Generators._
  "inserted and retrieved elements have same elements of a concurrent map" in new TestContext {
    forAll { (commands: List[Operation]) =>
      withHyperMap { (memoryFileHyperMap, concurrentHashMap) =>
        commands.foreach {
          case Put(key, value) => //Future {
            memoryFileHyperMap.put(key, value)
            concurrentHashMap.put(key, value)
          //}
          case Delete(key) => //Future {
            memoryFileHyperMap.remove(key)
            concurrentHashMap.remove(key)
          //}
        }
        println(memoryFileHyperMap.hyperMapSize)
        memoryFileHyperMap.keyValues.size should ===(memoryFileHyperMap.size())
        memoryFileHyperMap.size() should ===(concurrentHashMap.size())
        memoryFileHyperMap.keyValues should contain theSameElementsAs concurrentHashMap.keys().asScala.toList
      }
    }
  }


  trait TestContext {
    protected val moveToMemoryLimit = 5
    protected val moveToFileLimit   = 6

    def withHyperMap(fn: (MemoryFileHyperMap[Key, Value], ConcurrentHashMap[Key, Value]) => Unit) = {
      val concurrentHashMap = new ConcurrentHashMap[Key, Value]()
      Lifecycle.using(MemoryFileHyperMapFactory.make[Key, Value](moveToMemoryLimit, moveToFileLimit)) { memoryMap =>
        fn(memoryMap, concurrentHashMap)
      }
    }
  }

}

case class Key(keyString: String)

case class Value(valueInt: Int)
