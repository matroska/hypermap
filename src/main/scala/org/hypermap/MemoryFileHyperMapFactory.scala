package org.hypermap

import java.io.File
import java.util.concurrent.ConcurrentMap

import com.itv.lifecycle.{Lifecycle, VanillaLifecycle}
import org.mapdb.{DB, DBMaker, Serializer}

import scala.collection.mutable.Map

object MemoryFileHyperMapFactory {

  def make[K, V](moveToMemoryLimit: Long,
                 moveToFileLimit: Long): Lifecycle[MemoryFileHyperMap[K, V]] =
    new VanillaLifecycle[MemoryFileHyperMap[K, V]] {
      private val fileName = "exampleFile.db"
      new File(fileName).delete()

      val memoryMap: Map[K, V] = scala.collection.mutable.HashMap.empty
      val db: DB = DBMaker
        .fileDB(fileName)
        .fileMmapEnable()
        .fileDeleteAfterClose()
        .closeOnJvmShutdown()
        .make

      val fileMap: ConcurrentMap[K, V] = db
        .hashMap("map", Serializer.ELSA, Serializer.ELSA)
        .create()
        .asInstanceOf[ConcurrentMap[K, V]]

      def start(): MemoryFileHyperMap[K, V] =
        new MemoryFileHyperMap[K, V](memoryMap, fileMap, moveToMemoryLimit, moveToFileLimit)

      def shutdown(instance: MemoryFileHyperMap[K, V]): Unit = db.close()
    }
}
