package org.hypermap

import java.util.concurrent.ConcurrentMap
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantReadWriteLock

private[hypermap] class MemoryFileHyperMap[K, V](private[hypermap] val memoryMap: scala.collection.mutable.Map[K, V],
                                                 private[hypermap] val fileMap: ConcurrentMap[K, V],
                                                 moveToMemoryLimit: Long,
                                                 moveToFileLimit: Long)
    extends HyperMap[K, V] {

  private val (readLock, writeLock) = {
    val reentrantReadWriteLock = new ReentrantReadWriteLock()
    (reentrantReadWriteLock.readLock(), reentrantReadWriteLock.writeLock())
  }

  def put(key: K, value: V): Unit = {
    withReadLock {
      if (memoryMap.contains(key)) {
        memoryMap.put(key, value)
      } else if (usingFile.get() && fileMap.containsKey(key)) {
        fileMap.put(key, value)
      } else if (mustPutInFileMap) {
        fileMap.put(key, value)
        usingFile.set(true)
      } else memoryMap.put(key, value)
    }
  }

  def get(key: K): Option[V] = {
    memoryMap.get(key).orElse(Option(fileMap.get(key)))
  }

  import scala.collection.JavaConverters._
  //locks only put or remove operations
  def remove(key: K): Option[V] = {
    readLock.lock()
    val output = memoryMap.remove(key).orElse(Option(fileMap.remove(key)))
    if (output.isDefined && shouldCompact) {
      readLock.unlock()
      compact()
    } else readLock.unlock()
    output
  }

  private def compact() = {
    try {
      writeLock.lock()
      // lock every operation and move all the elements from fileMap to memoryMap
      fileMap.entrySet().iterator().asScala.foreach { entry =>
        memoryMap += entry.getKey -> entry.getValue
      }
      fileMap.clear()
      usingFile.set(false)
    } finally { writeLock.unlock() }
  }

  def hyperMapSize: HyperMapSize = withReadLock(HyperMapSize(memoryMap.keySet.size, fileMap.keySet().size()))

  def keyValues = memoryMap.keys ++ fileMap.keySet().asScala

  override def size(): Long = keyValues.size

  def clear(): Unit = {
    withReadLock {
      memoryMap.clear()
      fileMap.clear()
    }
  }

  private def withReadLock[T](fn: => T) = {
    try {
      readLock.lock()
      fn
    } finally { readLock.unlock() }
  }
  private def totalSize                = memoryMap.size + fileMap.size()
  private def mustPutInFileMap    = memoryMap.size >= moveToFileLimit
  private val usingFile: AtomicBoolean = new AtomicBoolean(false)
  private def shouldCompact = totalSize == moveToMemoryLimit
}

trait HyperMap[K, V] {

  def put(key: K, value: V): Unit

  def +=(tuple: (K, V)): Unit = put(tuple._1, tuple._2)

  def -=(key: K): Unit = remove(key)

  def get(key: K): Option[V]

  def size(): Long

  def remove(key: K): Option[V]

  def clear(): Unit

}

case class HyperMapSize(memoryMapSize: Int, fileMapSize: Int) {
  def totalSize: Int = memoryMapSize + fileMapSize
}
