name := "FileMap"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies += "org.mapdb" % "mapdb" % "3.0.5"
libraryDependencies += "com.itv" %% "lifecycle" % "0.3"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.4" % "test"
libraryDependencies += "com.github.alexarchambault" %% "scalacheck-shapeless_1.13" % "1.1.6" % "test"